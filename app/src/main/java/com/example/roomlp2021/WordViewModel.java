package com.example.roomlp2021;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WordViewModel extends AndroidViewModel {
    private WordRepository repository;
    private LiveData<List<Word>> wordsLD;
    private LiveData<Integer> nbMotsLD;

    public LiveData<List<Word>> getWordsLD() {
        return wordsLD;
    }

    public LiveData<Integer> getNbMotsLD() {
        return nbMotsLD;
    }

    public WordViewModel(@NonNull Application application) {
        super(application);
        repository = new WordRepository(application);
        wordsLD = repository.getMyListWords();
        nbMotsLD = repository.getNmElemLD();

    }

    public void insert(Word word){
        repository.insert(word);
    }

    public void deleteAll(){
        repository.deleteAll();
    }

    public Integer nbElements(){
        return repository.getNbElements();
    }
}
