package com.example.roomlp2021;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WordRepository {
    private WordDAO myWordDAO;
    private LiveData<List<Word>> myListWords;
    private LiveData<Integer> nmElemLD;

    public WordRepository(Application application){
        WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
        myWordDAO = db.wordDAO();
        myListWords = myWordDAO.getAllWords();
        nmElemLD = myWordDAO.nbElementsLD();
    }

    public LiveData<List<Word>> getMyListWords() {
        return myListWords;
    }

    public LiveData<Integer> getNmElemLD() {
        return nmElemLD;
    }

    public void insert(Word word){
        new insertAsyncTask(myWordDAO).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Word,Void,Void> {
        private WordDAO dao;
        public insertAsyncTask(WordDAO myWordDAO) {
            dao = myWordDAO;
        }

        @Override
        protected Void doInBackground(Word... words) {
            dao.insert(words[0]);
            return null;
        }
    }

    public void deleteAll(){
        new deleteAllAsyncTask(myWordDAO).execute();
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void,Void,Void> {
        WordDAO dao;
        public deleteAllAsyncTask(WordDAO myWordDAO) {
            dao = myWordDAO;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.deleteAll();
            return null;
        }
    }

    public Integer getNbElements(){
        try{
            return new getNbElementsAsyncTask(myWordDAO).execute().get();
        }catch(Exception e){
            Log.d("MesLogs","Erreur de getNbElements");
        }
        return null;
    }

    private static class getNbElementsAsyncTask extends AsyncTask<Void,Void,Integer>{
        WordDAO dao;
        public getNbElementsAsyncTask(WordDAO myWordDAO) {
            dao = myWordDAO;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return new Integer(dao.nbElements());
        }
    }
}
